﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Мой сайт</title>
    <link rel="stylesheet" href="Style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/bootstrap-4.5.2-dist/css/bootstrap.css">
</head>
<body>
    <div class="top-line">
        <div class="Rectangle_1">
            <img class="phone-outline" src="/img/eva_phone-outline.svg">
        </div>
    </div>
    <div class="secon_line">
        <div class="Rectangle_2"></div>
    </div>
    <div class="footer">
        &copy; Александр Ломтев
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/bootstrap/bootstrap-4.5.2-dist/js/bootstrap.min.js"></script>
</body>
</html>